//! Rusty-binder attempts to provide a framework to generate language bindings from Rust files.
//!
//! **A note on versioning:** While rusty-binder is still in a significant flux (i.e. pre-`v1.0.0`)
//! it will likely go through numerous breaking changes. However, until `v1.0.0`, any time a
//! breaking change is made the minor version will be bumped and any time a new feature is added
//! the path version will be bumped. If you find that this is not the case then please open an
//! issue at the [repo](https://gitlab.com/rusty-binder/rusty-binder).
//!
//! Rusty-binder is a language-agnostic bindings generator which is designed to use
//! language-specific external crates in a build script. A typical use might look something like
//! this:
//!
//! ```toml
//! # Cargo.toml
//!
//! [package]
//! build = "build.rs"
//!
//! [build-dependencies]
//! rusty-binder = "..."
//! # NOTE THAT THESE TWO CRATES (`rusty-python` and `rusty-ruby`) HAVE NOT YET BEEN WRITTEN,
//! # THEY ARE JUST BEING USED AS HYPOTHETICAL EXAMPLES.
//! rusty-python = "..."
//! rusty-ruby = "..."
//!
//! [lib]
//! crate-type = ["dylib"]
//! ```
//!
//! ```ignore
//! // build.rs
//!
//! extern crate binder;
//! extern crate python;
//! extern crate ruby;
//!
//! fn main() {
//!     let python = python::Generator(...);
//!     let ruby = ruby::Generator(...);
//!
//!     binder::Binder::new().expect("could not read manifest")
//!         .module("capi").expect("malformed path")
//!         .output_directory("my_bindings")
//!         .register(python)
//!         .register(ruby)
//!         .run_build();
//! }
//! ```
//!
//! After this there would be Python bindings available in `my_bindings/python` and Ruby bindings
//! available in `my_bindings/ruby` (or locations similar to that).

extern crate syntex_syntax as syntax;
extern crate syntex_errors as errors;
extern crate syntex_pos;
extern crate toml;

pub mod compiler;
mod parse;

use parse::Generator;
use std::convert;
use std::io::Read;
use std::io::Write;
use std::path;


/// Store the source code.
#[derive(Debug)]
enum Source {
    String(String),
    File(path::PathBuf),
}

impl Source {
    fn parse_crate(&self, sess: &compiler::session::Session) -> std::io::Result<syntax::ast::Crate> {
        match *self {
            Source::File(ref path) => compiler::session::parse_crate_from_file(path, sess),
            Source::String(ref source) => compiler::session::parse_crate_from_source_str(
                "binder_source".to_owned(),
                source.clone(),
                sess,
            ),
        }
    }
}


/// The type responsible for managing the generators of language bindings.
pub struct Binder {
    /// The Rust source code.
    input: Source,
    /// The directory in which to place all code.
    pub output_directory: std::path::PathBuf,
    session: compiler::session::Session,
    generators: Vec<Generator>,
}

pub type Language = String;

/// A compiled binding which has not yet been written to a file.
#[derive(Debug)]
pub struct Binding {
    /// The language that this binding is for.
    pub language: Language,
    /// The files that make up this language's binding.
    pub files: Vec<compiler::File>,
    /// Any language bindings that this binding relies on.
    pub dependencies: Vec<Binding>,
}


impl Binder {
    /// Create a new Binder.
    ///
    /// This can only fail if there are issues reading the cargo manifest. If there is no cargo
    /// manifest available then the source file defaults to `src/lib.rs`.
    pub fn new() -> std::result::Result<Binder, std::io::Error> {
        let source_path = try!(source_file_from_cargo());
        let input = Source::File(path::PathBuf::from(source_path));

        Ok(Binder {
            input: input,
            output_directory: path::Path::new("target").join("binder"),
            session: compiler::session::Session::default(),
            generators: vec![],
        })
    }

    /// Set the path to the root source file of the crate.
    ///
    /// This should only be used when not using a `cargo` build system.
    pub fn source_file<T>(&mut self, path: T) -> &mut Binder
        where path::PathBuf: convert::From<T>,
    {
        self.input = Source::File(path::PathBuf::from(path));
        self
    }

    /// Set a string to be used as source code.
    ///
    /// Currently this should only be used with small strings as it requires at least one `.clone()`.
    pub fn source_string(&mut self, source: &str) -> &mut Binder {
        self.input = Source::String(source.to_owned());
        self
    }

    /// Set the directory in which to place all output bindings.
    ///
    /// This defaults to `target/binder`, and the output from each language will be put in it's own
    /// folder.
    pub fn output_directory<T>(&mut self, path: T) -> &mut Binder
        where path::PathBuf: convert::From<T>,
    {
        self.output_directory = path::PathBuf::from(path);
        self
    }

    /// Set the module which contains the API which is to be bound.
    ///
    /// The module should be described using Rust's path syntax, i.e. in the same way that you
    /// would `use` the module (`"path::to::api"`).
    ///
    /// # Fails
    ///
    /// If the path is malformed (e.g. `path::to:module`).
    pub fn module(&mut self, module: &str) -> Result<&mut Binder, ()> {
        let sess = syntax::parse::ParseSess::new();
        let mut parser = syntax::parse::new_parser_from_source_str(
            &sess,
            "".into(),
            module.into(),
        );

        let path = parser.parse_path(syntax::parse::parser::PathStyle::Mod);
        // ensure no extra tokens (can happen when `module` is `"c:api"` or similar)
        let eof = parser.bump_and_get();

        if let Ok(path) = path {
            if eof == syntax::parse::token::Token::Eof {
                self.session.module = Some(path);
                return Ok(self)
            }
        }

        self.session.err(&format!("malformed module path {:?}", module));
        Err(())
    }

    /// Register a compiler to be used to generate bindings.
    pub fn register<C: compiler::Compiler + 'static>(&mut self, compiler: C) -> &mut Binder {
        let dependencies = compiler.dependencies();
        self.generators.push(Generator::new(Box::new(compiler), dependencies));
        self
    }

    /// Compile the bindings to strings.
    ///
    /// # Fails
    ///
    /// If there was an error during compilation.
    pub fn compile(&mut self) -> Result<Vec<Binding>, std::io::Error> {
        let krate = try!(self.input.parse_crate(&self.session));
        parse::parse_crate(&krate, &mut self.session, &mut self.generators)
    }

    /// Get the errors which have occurred during compilation.
    pub fn errors(&self) -> String {
        self.session.errors()
    }

    /// Compile the bindings and write them to the appropriate files.
    ///
    /// # Fails
    ///
    /// If there was an error during compilation or writing to file. **Note:** this function will
    /// not print errors on failure, you must retrieve them yourself using `Binder.errors`;
    /// alternatively use `Binder.run_build` which will print errors on failure.
    pub fn write(&mut self) -> Result<(), std::io::Error> {
        let bindings = try!(self.compile());
        write_binding(&self.output_directory, &bindings)
    }

    /// Build script utility function.
    ///
    /// Compile the bindings, if there were any errors print them to stderr and panic, otherwise
    /// write the bindings to the file.
    pub fn run_build(&mut self) {
        match self.compile() {
            Ok(ref bindings) => write_binding(&self.output_directory, bindings).expect("io error"),
            Err(err) => {
                writeln!(&mut std::io::stderr(), "{}", self.errors())
                    .expect("unable to write to stderr");
                panic!(err);
            },
        }
    }
}

/// Recursively write bindings and their dependencies to the file system.
fn write_binding(root: &path::Path, bindings: &[Binding]) -> Result<(), std::io::Error> {
    for binding in bindings {
        let out_dir = root.join(&binding.language);
        try!(std::fs::create_dir_all(&out_dir));

        for file in &binding.files {
            let mut f = try!(std::fs::File::create(&out_dir.join(&file.path)));
            try!(f.write_all(file.contents.as_bytes()));
        }

        let dependencies_root = out_dir.join("dependencies");
        try!(write_binding(&dependencies_root, &binding.dependencies));
    }

    Ok(())
}

/// Extract the path to the root source file from a `Cargo.toml`.
fn source_file_from_cargo() -> std::result::Result<String, std::io::Error> {
    let cargo_toml = path::Path::new(
        &std::env::var_os("CARGO_MANIFEST_DIR")
        .unwrap_or_else(|| std::ffi::OsString::from(""))
    ).join("Cargo.toml");

    // If no `Cargo.toml` assume `src/lib.rs` until told otherwise.
    let default = "src/lib.rs";
    let mut cargo_toml = match std::fs::File::open(&cargo_toml) {
        Ok(value) => value,
        Err(..) => return Ok(default.to_owned()),
    };

    let mut buf = String::new();
    try!(cargo_toml.read_to_string(&mut buf));

    let table = match buf.parse::<toml::Value>() {
        Ok(t) => t,
        Err(e) => return Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!("cargo manifest could not be parsed: {}", e),
        )),
    };

    // If not explicitly stated then defaults to `src/lib.rs`.
    Ok(table.get("lib")
       .and_then(|t| t.get("path"))
       .and_then(|s| s.as_str())
       .unwrap_or(default)
       .into())
}
