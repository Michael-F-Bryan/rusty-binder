//! Useful functions for obtaining information from the AST.
//!
//! If you can think of any other functionality which would be useful here, please open an issue on
//! the [repo](https://gitlab.com/rusty-binder/rusty-binder). Consider examining the
//! `syntex_syntax::ast_util` module for ideas.

use compiler::session;
pub use compiler::session::has_c_repr;
use compiler::Stop;
use syntax;
use syntax::ast;
pub use syntax::print::pprust::pat_to_string;
pub use syntax::print::pprust::ty_to_string;
pub use syntax::print::pprust::variant_to_string;

/// Obtain the docstring from an item's attributes.
///
/// The comment characters (`///`, `/**`, or `*/`) and any remaining spaces are stripped from the
/// left- and right- sides of the lines before prepending `prepend` and appending `append` to each
/// line.
///
/// # Note
///
/// This currently only works with `///`-style doc comments.
pub fn docs_from_attrs(attrs: &[ast::Attribute], prepend: &str, append: &str) -> String {
    let mut buffer = String::new();

    for attr in attrs {
        let name = attr.value.name;
        match attr.value.node {
            ast::MetaItemKind::NameValue(ref val) if name == "doc" => match val.node {
                // Docstring attributes omit the trailing newline.
                ast::LitKind::Str(docs, _) => {
                    let docs = &docs.as_str()[3..];
                    buffer.push_str(&format!(
                        "{}{}{}\n",
                        prepend,
                        docs.trim(),
                        append,
                    ))
                },
                _ => unreachable!("docs must be literal strings"),
            },
            _ => {},
        }
    }

    buffer
}

/// Get the string representation of an `Ident`.
///
/// This is currently wasteful as it returns a `String` instead of a `&str`. You can get around
/// this by replacing a call to this function with:
///
/// ```ignore
/// let name: &str = &ident.name.as_str();
/// ```
pub fn name_from_ident(ident: &ast::Ident) -> String {
    let as_str: &str = &ident.name.as_str();
    as_str.into()
}

/// Get the name of a function argument declaration.
///
/// This is required because function arguments can not be guaranteed to be an identifier.
pub fn arg_name_from_pattern(session: &mut session::Session, pat: &ast::Pat) -> Result<String, Stop> {
    use syntax::ast::{PatKind, BindingMode};
    match pat.node {
        PatKind::Ident(BindingMode::ByValue(_), ref ident, _) => {
            let as_str: &str = &ident.node.name.as_str();
            Ok(as_str.into())
        },
        PatKind::Ident(..) => Err(session.span_err(
            pat.span,
            "binder does not support by-ref arguments",
        )),
        _ => Err(session.span_bug(
            pat.span,
            "`arg_name_from_pattern` was called on a pattern which isn't a function argument",
        )),
    }
}

/// Extracts a non-generic `Ty` from an `Item`.
pub fn ty_from_item<'i>(session: &mut session::Session, item: &'i ast::Item) -> Result<&'i ast::Ty, Stop> {
    match item.node {
        ast::ItemKind::Ty(ref ty, ref generics) => {
            if generics.is_parameterized() {
                Err(Stop::Abort)
            } else {
                Ok(&*ty)
            }
        },
        _ => Err(session.span_bug_with_note(
            item.span,
            "`ty_from_item` was called on an incorrect item",
            "expected `syntax::ast::Item_::ItemTy`",
        )),
    }
}

/// Extracts a non-generic `EnumDef` from an `Item`.
pub fn enum_from_item<'i>(session: &mut session::Session, item: &'i ast::Item) -> Result<&'i ast::EnumDef, Stop> {
    match item.node {
        ast::ItemKind::Enum(ref enum_def, ref generics) => {
            if generics.is_parameterized() {
                Err(Stop::Abort)
            } else {
                Ok(enum_def)
            }
        },
        _ => Err(session.span_bug_with_note(
            item.span,
            "`enum_from_item` was called on an incorrect item",
            "expected `syntax::ast::Item_::ItemEnum`",
        )),
    }
}

/// Extract the fields from a non-generic struct.
pub fn struct_from_item<'f>(session: &mut session::Session, item: &'f ast::Item) -> Result<&'f [ast::StructField], Stop> {
    match item.node {
        ast::ItemKind::Struct(ast::VariantData::Struct(ref fields, _), ref generics) |
        ast::ItemKind::Struct(ast::VariantData::Tuple(ref fields, _), ref generics) => {
            if generics.is_parameterized() {
                Err(Stop::Abort)
            } else {
                Ok(fields)
            }
        },
        ast::ItemKind::Struct(..) => Err(session.span_err(
            item.span,
            "unit structs have no C equivalent",
        )),
        _ => Err(session.span_bug_with_note(
            item.span,
            "`struct_from_item` was called on an incorrect item",
            "expected `syntax::ast::ItemKind::Struct`",
        )),
    }
}

/// Extract a non-generic `FnDecl` of C ABI.
pub fn fn_from_item<'i>(session: &mut session::Session, item: &'i ast::Item) -> Result<&'i ast::FnDecl, Stop> {
    match item.node {
        ast::ItemKind::Fn(ref fn_decl, _, _, abi, ref generics, _) => {
            if generics.is_parameterized() || !is_c_abi(abi) {
                Err(Stop::Abort)
            } else {
                Ok(fn_decl)
            }
        },
        _ => Err(session.span_bug_with_note(
            item.span,
            "`fn_decl_from_item` was called on an incorrect item",
            "expected `syntax::ast::Item_::ItemFn`",
        )),
    }
}

/// Apply the closure `f` to each variant of an enum.
///
/// # Fails
///
/// If any variant is a non-unit enum variant (as this is not FFI-safe).
pub fn for_each_variant<F>(session: &mut session::Session, definition: &ast::EnumDef, mut f: F) -> Result<(), Stop>
// must be FnMut so that implementations can use mutable buffers in the closure
where F: FnMut(&ast::Variant_) {
    for var in &definition.variants {
        if !var.node.data.is_unit() {
            return Err(session.span_err(
                var.span,
                "non-unit enum variants have no C equivalent",
            ))
        }

        f(&var.node);
    }

    Ok(())
}


/// Check whether the given path type contains any parameters.
pub fn ty_is_generic(ty: &ast::Ty) -> bool {
    match ty.node {
        ast::TyKind::Path(_, ref path) => path.segments.iter().any(
            |segment| !segment.parameters.is_none()
        ),
        _ => false,
    }
}

// TODO: implement this when fixing #30.
// /// Check whether the given path type contains any parameters.
// ///
// /// The exception is a nullable function pointer which is represented as
// ///
// /// ```ignore
// /// Option<extern fn(...) -> ...>
// /// ```
// pub fn ty_is_generic_not_null_fn_ptr(ty: &ast::Ty) -> bool {
//     false
// }

/// Check whether any fields of a struct are public.
///
/// If all struct fields are private then the struct will be represented as an opaque struct.
pub fn is_opaque(fields: &[ast::StructField]) -> bool {
    fields.iter().all(
        |field| match field.vis {
            ast::Visibility::Public => false,
            _ => true,
        }
    )
}

/// Check that an `Item` has been marked `#[no_mangle]`.
pub fn is_no_mangle(item: &ast::Item) -> bool {
    syntax::attr::contains_name(&item.attrs, "no_mangle")
}

/// Check whether the ABI is a C ABI.
pub fn is_c_abi(abi: syntax::abi::Abi) -> bool {
    use syntax::abi::Abi;
    match abi {
        Abi::C | Abi::Cdecl | Abi::Stdcall | Abi::Fastcall | Abi::System => true,
        _ => false,
    }
}
