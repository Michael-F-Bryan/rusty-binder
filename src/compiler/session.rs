//! The type used to store state and conveniences for parsing and compiling.

use compiler::Stop;
use std::default::Default;
use std::io::{Result, Write};
use std::sync::{Arc, Mutex};
use syntax;
use syntax::ast;
use syntax::codemap::Span;

/// A Send object which implements Write.
///
/// Errors are printed into this instead of stderr. We need Arc to get Send with multiple
/// ownership, and Mutex to get Sync for Vec<u8> (which has Write).
#[derive(Debug, Clone)]
struct ErrorBuffer(Arc<Mutex<Vec<u8>>>);

impl Default for ErrorBuffer {
    fn default() -> ErrorBuffer {
        ErrorBuffer(Arc::new(Mutex::new(Vec::new())))
    }
}

impl Write for ErrorBuffer {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let mut inner_buffer = self.0.lock().unwrap();
        inner_buffer.write(buf)
    }

    fn flush(&mut self) -> Result<()> {
        let mut inner_buffer = self.0.lock().unwrap();
        inner_buffer.flush()
    }
}


/// Store options and state required for parsing and compiling.
pub struct Session {
    /// The actual session used to emit errors, etc.
    syntax_session: ::syntax::parse::ParseSess,
    /// The module which contains the C API.
    pub module: Option<ast::Path>,
    // TODO:
    // /// The name of the compiler currently being used.
    // ///
    // /// This is used to give better error messages.
    // compiler: String,
    /// A buffer into which the errors are written.
    ///
    /// This prevents them being printed to stderr.
    error_buffer: ErrorBuffer,
}

impl Default for Session {
    /// Construct a new empty session.
    fn default() -> Session {
        use std::rc::Rc;
        use syntax::codemap::CodeMap;
        use errors::emitter::EmitterWriter;
        use errors::Handler;
        use syntax::parse::ParseSess;

        let error_buffer = ErrorBuffer::default();
        let code_map = Rc::new(CodeMap::new());
        let emitter = Box::new(EmitterWriter::new(
            Box::new(error_buffer.clone()),
            Some(code_map.clone()),
        ));
        let syntax_session = ParseSess::with_span_handler(
            Handler::with_emitter(true, false, emitter),
            code_map.clone(),
        );

        Session {
            syntax_session: syntax_session,
            module: None,
            // compiler: String::new(),
            error_buffer: error_buffer,
        }
    }
}

impl Session {
    /// Get the errors that have occurred as a &str.
    ///
    /// This is what would have been printed to stderr up until the point you call the function.
    pub fn errors(&self) -> String {
        let inner_buffer = self.error_buffer.0.lock().unwrap();
        // All data comes from strings originally so must be valid UTF8.
        unsafe { String::from_utf8_unchecked(inner_buffer.clone()) }
    }

    pub fn bug(&self, msg: &str) -> Stop {
        // `Handler.bug` panics so we need to do it manually.
        self.syntax_session
            .span_diagnostic
            .emit(&::syntex_pos::MultiSpan::new(), msg, ::errors::Level::Bug);
       self.syntax_session.span_diagnostic.bump_err_count();
        Stop::Fail
    }

    pub fn span_bug(&self, span: Span, msg: &str) -> Stop {
        self.syntax_session.span_diagnostic.span_bug_no_panic(span, msg);
        Stop::Fail
    }

    pub fn span_bug_with_note(&self, span: Span, msg: &str, note: &str) -> Stop {
        self.syntax_session.span_diagnostic.span_bug_no_panic(span, msg);
        self.syntax_session.span_diagnostic.span_note_without_error(span, note);
        Stop::Fail
    }

    #[allow(unused_must_use)]
    pub fn fatal(&self, msg: &str) -> Stop {
        self.syntax_session.span_diagnostic.fatal(msg);
        Stop::Fail
    }

    pub fn fatal_with_help(&self, msg: &str, help: &str) -> Stop {
        self.syntax_session.span_diagnostic.struct_fatal(msg)
            .help(help)
            .emit();
        Stop::Fail
    }

    #[allow(unused_must_use)]
    pub fn span_fatal(&self, span: Span, msg: &str) -> Stop {
        self.syntax_session.span_diagnostic.span_fatal(span, msg);
        Stop::Fail
    }

    pub fn err(&self, msg: &str) -> Stop {
        self.syntax_session.span_diagnostic.err(msg);
        Stop::Fail
    }

    pub fn span_err(&self, span: Span, msg: &str) -> Stop {
        self.syntax_session.span_diagnostic.span_err(span, msg);
        Stop::Fail
    }

    pub fn warn(&self, msg: &str) {
        self.syntax_session.span_diagnostic.warn(msg);
    }

    pub fn span_warn(&self, span: Span, msg: &str) {
        self.syntax_session.span_diagnostic.span_warn(span, msg);
    }

    pub fn note(&self, msg: &str) {
        self.syntax_session.span_diagnostic.note_without_error(msg);
    }

    pub fn span_note(&self, span: Span, msg: &str) {
        self.syntax_session.span_diagnostic.span_note_without_error(span, msg);
    }
}

/// Check that an item has a C-compatible representation.
pub fn has_c_repr(item: &ast::Item, session: &mut Session) -> bool {
    item.attrs.iter().any(
        |attr| syntax::attr::find_repr_attrs(&session.syntax_session.span_diagnostic, attr).iter().any(
            |repr| repr.is_ffi_safe()
        )
    )
}

/// Parse a crate from a source file.
pub fn parse_crate_from_file(path: &::std::path::Path, sess: &Session) -> Result<syntax::ast::Crate> {
    match syntax::parse::parse_crate_from_file(path, &sess.syntax_session) {
        Ok(krate) => Ok(krate),
        Err(mut diag) => {
            diag.emit();
            Err(::std::io::Error::new(::std::io::ErrorKind::Other, "parse error"))
        },
    }
}

/// Parse a crate from a source string.
pub fn parse_crate_from_source_str(name: String, source: String, sess: &Session) -> Result<syntax::ast::Crate> {
    match syntax::parse::parse_crate_from_source_str(name, source, &sess.syntax_session) {
        Ok(krate) => Ok(krate),
        Err(mut diag) => {
            diag.emit();
            Err(::std::io::Error::new(::std::io::ErrorKind::Other, "parse error"))
        }
    }
}
